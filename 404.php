<?php get_header(); ?>
		<!-- <main> -->
				<div id="content" class="container mt-3" role="main">
					<div id="error" class="row">
						<div class="col-12">
							<h1><?php _e("404, Page not found", ZEETEXTDOMAIN); ?></h1>
							<p><?php _e("The page you are looking for doesn't exist or another error has occurred.", ZEETEXTDOMAIN); ?></p>
							<a class="btn btn-success btn-sm" href="<?php echo home_url(); ?>"><?php _e("GO BACK TO THE HOMEPAGE", ZEETEXTDOMAIN); ?></a>
						</div>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
