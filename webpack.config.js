const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const entry = {
	main: [
		"./sass/main.scss"
		,"./typescript/main.ts"
	]
};

const export_module = {
	rules: [{
		test: /\.ts$/
		,use: "ts-loader"
	}
	,{
		test: /\.s[ac]ss$/i
		,use: [
			"style-loader"
			,"css-loader"
			,"sass-loader"
		]
	}]
};

const resolve = {
	extensions: [".ts", ".js", ".scss"]
};

const output_filename = "assets/js/[name].min.js";

const output_library = "alttheme";

const optimization = {
	minimize: true
	,minimizer: [new TerserPlugin({
		terserOptions: {
			mangle: false
		}
	})]
};

const plugins = [
	new CopyPlugin({
		patterns: [
			{	from: "*.php"
				,to: ""
			},{	from: "*.css"
				,to: ""
			},{ from: "COPYING"
				,to: ""
			},{ from: "favicon.ico"
				,to: ""
			},{	from: "screenshot.png"
				,to: ""
			},{	from: "admin"
				,to: "admin"
			},{	from: "assets/favicon"
				,to: "assets/favicon"
			},{	from: "assets/images"
				,to: "assets/images"
			},{	from: "languages"
				,to: "languages"
			},{	from: "lib"
				,to: "lib"
			},{	from: "post-templates"
				,to: "post-templates"
			},{	from: "node_modules/@popperjs/core/dist/umd/popper.min.js"
				,to: "assets/js/popper.min.js"
			},{	from: "node_modules/bootstrap/dist/js/bootstrap.min.js"
				,to: "assets/js/bootstrap.min.js"
			},{	from: "node_modules/open-iconic/font/fonts"
				,to: "assets/fonts"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-100-normal.woff"
				,to: "assets/css/files/roboto-latin-100-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-100-normal.woff2"
				,to: "assets/css/files/roboto-latin-100-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-100-normal.woff"
				,to: "assets/css/files/roboto-latin-ext-100-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-100-normal.woff2"
				,to: "assets/css/files/roboto-latin-ext-100-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-300-normal.woff"
				,to: "assets/css/files/roboto-latin-300-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-300-normal.woff2"
				,to: "assets/css/files/roboto-latin-300-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-300-normal.woff"
				,to: "assets/css/files/roboto-latin-ext-300-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-300-normal.woff2"
				,to: "assets/css/files/roboto-latin-ext-300-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-400-normal.woff"
				,to: "assets/css/files/roboto-latin-400-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-400-normal.woff2"
				,to: "assets/css/files/roboto-latin-400-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-400-normal.woff"
				,to: "assets/css/files/roboto-latin-ext-400-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-400-normal.woff2"
				,to: "assets/css/files/roboto-latin-ext-400-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-500-normal.woff"
				,to: "assets/css/files/roboto-latin-500-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-500-normal.woff2"
				,to: "assets/css/files/roboto-latin-500-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-500-normal.woff"
				,to: "assets/css/files/roboto-latin-ext-500-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-500-normal.woff2"
				,to: "assets/css/files/roboto-latin-ext-500-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-700-normal.woff"
				,to: "assets/css/files/roboto-latin-700-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-700-normal.woff2"
				,to: "assets/css/files/roboto-latin-700-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-700-normal.woff"
				,to: "assets/css/files/roboto-latin-ext-700-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-700-normal.woff2"
				,to: "assets/css/files/roboto-latin-ext-700-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-900-normal.woff"
				,to: "assets/css/files/roboto-latin-900-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-900-normal.woff2"
				,to: "assets/css/files/roboto-latin-900-normal.woff2"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-900-normal.woff"
				,to: "assets/css/files/roboto-latin-ext-900-normal.woff"
			},{	from: "node_modules/@fontsource/roboto/files/roboto-latin-ext-900-normal.woff2"
				,to: "assets/css/files/roboto-latin-ext-900-normal.woff2"
			},{	from: "node_modules/bootstrap/dist/css/bootstrap.min.css"
				,to: "assets/css/bootstrap.min.css"
			},{	from: "node_modules/open-iconic/font/css/open-iconic.min.css"
				,to: "assets/css/open-iconic.min.css"
			},{	from: "node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css"
				,to: "assets/css/open-iconic-bootstrap.min.css"
			},{	from: "node_modules/animate.css/animate.min.css"
				,to: "assets/css/animate.min.css"
			},{	from: "node_modules/@fontsource/roboto/latin.css"
				,to: "assets/css/latin.css"
			},{	from: "node_modules/@fontsource/roboto/latin-ext.css"
				,to: "assets/css/latin-ext.css"
			}
		]
	})
]

const dist = {
	entry: entry
	,name: "dist"
	,mode: "production"
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dist")
		,library: output_library
	}
	,optimization: optimization
	,plugins: plugins
};

const dev = {
	entry: entry
	,name: "dev"
	,mode: "development"
	,devtool: "source-map"
	,devServer: {
		static: ["dev"],
		devMiddleware: { writeToDisk: true },
		proxy: { "/": { target: "http://localhost:80/" } },
		open: true
	}
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dev")
		,library: output_library
	}
	,optimization: optimization
	,plugins: plugins
}

module.exports = [ dist, dev ];
