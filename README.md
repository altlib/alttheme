# AltTheme

## Description
This is a WordPress theme based on FlatTheme by The ShapeBootstrap team.

## Dependencies
WP Bootstrap Navwalker - https://github.com/gilmorem560/wp-bootstrap-navwalker/tree/v4.3.0-oi

This is currently a modified version which allows for using OpenIconic icons in menus. I've submitted a pull request upstream but until that is in place, this theme will not work with the upstream version.

## Building
Simply issue **npm run build** to stage a build of the theme in ./dev. This will then be used by Docker or can be copied to a local WordPress site. Issue **npm run dist** to stage a build of the theme in ./dist for deploying to a public website.

## Using Docker
Docker compose will look for the following files in docker-files:

- 00-wordpress.sql - A SQL dump of an existing WordPress site for testing
- wp-content - Any additional wp-content needed

With these files in place, build alttheme then issue **npm run compose**

You should then be able to access your site at http://localhost.

## Using Webpack DevServer
Webpack DevServer will reload pages as changes are made to watched items. Run **npm run serve** to launch the DevServer in the development configuration. This will rebuild the site upon any code changes and promote them to ./dev. This will also launch a browser to the WordPress site running in the above Docker containers. This Docker configuration allows for reloading of files, so changes updated by DevServer will be visible to the Docker instance immediately. Run **npm run debug** to build the site, stand it up in Docker, then attach the DevServer to the build directory. This will effectively launch your browser on a copy of the the latest code. Additionally, the DevServer will trigger a reload in your browser upon detected changes.
