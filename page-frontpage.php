<?php
/**
 * Template Name: Frontpage
 */
$args = array(
	"post_type"		=> "zee_slider"
	,"orderby"		=> "menu_order"
	,"order"		=> "ASC");
$sliders = get_posts($args);
$total_sliders = count($sliders);

get_header(); 
?>
		<!-- <main> -->
				<section id="main-slider" class="m-0 p-0">
					<div class="carousel slide wet-asphalt" data-ride="carousel">
						<div class="carousel-inner">
<?php
							foreach ($sliders as $key => $slider) {
								$full_img           =   wp_get_attachment_image_src(get_post_thumbnail_id($slider->ID), "full");
								$slider_position    =   get_post_meta($slider->ID, "slider_position", true);
								$boxed              =   (get_post_meta($slider->ID, "slider_boxed", true) == "yes") ? "boxed" : "";
								$has_button         =   (get_post_meta($slider->ID, "slider_button_text", true) == "") ? false : true;
								$button             =   get_post_meta($slider->ID, "slider_button_text", true);
								$button_url         =   get_post_meta($slider->ID, "slider_button_url", true);
								$video_url          =   get_post_meta($slider->ID, "slider_video_link", true);
								$video_type         =   get_post_meta($slider->ID, "slider_video_type", true);
								$bg_image_url       =   get_post_meta($slider->ID, "slider_background_image", true);
								$background_image   =   "background-image: url(".wp_get_attachment_url($bg_image_url).")";
								$columns            =   false;
								
								if (!empty($image_url) || !empty($video_url))
									$columns		=	true;
								
								if ($full_img) {
									$embed_code     =	"<img class='img-fluid' src='".$full_img[0]."' alt=''>";
									$columns        =   true;
								}

								switch($video_type) {
									case "youtube":
										$embed_code = "<iframe width='640' height='480' src='//www.youtube.com/embed/".get_video_ID($video_url)."?rel=0' frameborder='0' allowfullscreen></iframe>";
										break;
									case "vimeo":
										$embed_code = "<iframe src='//player.vimeo.com/video/".get_video_ID($video_url)."?title=0&amp;byline=0&amp;portrait=0&amp;color=a22c2f' frameborder='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen=''></iframe>";
										break;
									default:
										break;
								}
?>
								<div class="item <?php echo ($key == 0) ? "active" : "" ?>" style="<?php echo ($bg_image_url) ? $background_image : ""; ?>">
									<div class="container-md">
										<div class="row justify-content-end">
											<div class="<?php echo($columns) ? "col-md-6" : "col-md-12" ?>">
												<div class="carousel-content centered <?php echo $slider_position; ?>">
													<h2 class="<?php echo $boxed; ?> animation animated-item-1">
														<?php echo $slider->post_title; ?>
													</h2>
													<p class="<?php echo $boxed; ?> animation animated-item-2">
														<?php echo do_shortcode($slider->post_content); ?>
													</p>
<?php
													if ($has_button) {
?>
														<br/>
														<a class="btn btn-md animation animated-item-3" href="<?php echo $button_url; ?>"><?php echo $button; ?></a>
<?php
													}
?>
												</div>
											</div>
										</div>
									</div>
								</div>
<?php
										
							}
?>
						</div>
					</div>
				</section>
				<div class="container">
<?php
					the_post();
					the_content();
?>
				</div>
		<!-- </main> -->
<?php get_footer();
