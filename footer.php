			</section>
		</div>
		<footer id="footer" class="midnight-blue">
			<div class="container-md">
				<div class="row">
					<div class="col-11">
						<?php show_footer(); ?>
					</div>
					<div class="col-1">
						<a id="gototop" class="gototop float-right" href="#"><i class="oi oi-chevron-top"></i></a>
					</div>
				</div>
			</div>
		</footer>
		<?php google_analytics();?>
		<?php wp_footer(); ?>
	</body>
</html>
