import * as $ from "jquery";
import "bootstrap";

export function ready(): void
{
    $("#main-slider.carousel").carousel({ interval: 8000 });
    $(".contact-form").on("submit", contact.send_message);
    $(".gototop").on("click", gototop.click);
}

namespace contact {
    export function send_message(): boolean
    {
        $.post(
            $(this).attr("action")
            ,post_message
            ,"json");

        return false;
    }

    function post_message(data: JQuery.PlainObject): void
    {
        $(this).prev().text(data.message).fadeIn().delay(3000).fadeOut();
    }
}

namespace gototop {
    export function click(event: JQuery.ClickEvent): boolean
    {
        event.preventDefault();
        $("html, body").animate({
            scrollTop: $("html, body").offset().top
        }, 500);
        return false
    }
}

export namespace navbar {
   export function toggle(): void
    {
        $("#main-navbar").toggle(400);
    }
}

$(ready);
