<?php get_header(); ?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row" role="main">
<?php
						while (have_posts()) {
							the_post();
?>
							<article id="post-<?php the_ID(); ?>" <?php post_class("col-12"); ?>>
<?php
								edit_post_link(__("Edit", ZEETEXTDOMAIN), '<small class="edit-link float-right">', '</small><div class="clearfix"></div>');
								if (has_post_thumbnail() && ! post_password_required()) {
?>
									<div class="entry-thumbnail">
										<?php the_post_thumbnail(); ?>
									</div>
<?php
								}
?>
								<div class="entry-content">
<?php
									the_content();
									zee_link_pages();
?>
								</div>
							</article>
<?php
							comments_page();
						}
?>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
