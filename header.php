<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<title><?php wp_title("|", true, "right"); ?></title>
		<meta charset="<?php bloginfo("charset"); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo("pingback_url"); ?>">
		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri() ?>/assets/js/html5shiv.js"></script>
		<script src="<?php echo get_template_directory_uri() ?>/assets/js/respond.min.js"></script>
		<![endif]-->
<?php
		theme_favicon();
		wp_head();
?>
	</head>
	<body <?php body_class("d-flex flex-column"); ?>>
		<div id="page-content" class="py-1">
			<nav id="header" class="navbar navbar-expand-lg navbar-dark fixed-top py-0" role="banner">
				<div class="container-lg">
					<?php theme_logo(); ?>
					<button class="btn btn-sm d-lg-none" type="button" onclick="alttheme.navbar.toggle();" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div id="main-navbar" class="navbar-collapse collapse">
						<form id="search-form" class="form-inline flex-grow" action="/" role="search">
								<input id="search-bar" class="form-control form-control-sm" name="s" type="text" placeholder="Search" aria-label="Search">
								<button type="submit" class="btn btn-sm text-light d-none d-lg-inline"><span class="oi oi-magnifying-glass"></span></button>
						</form>
<?php
						if (has_nav_menu("primary")) {
							wp_nav_menu(array(
							"theme_location"	=> "primary"
							,"depth"			=> 2
							,"container"		=> "div"
							,"container_class"	=> "ml-auto"
							,"menu_class"		=> "navbar-nav navbar-main text-light"
							,"fallback_cb"		=> "WP_Bootstrap_Navwalker::fallback"
							,"walker"			=> new WP_Bootstrap_Navwalker()));
						}
?>
					</div>
				</div>
			</nav>
			<?php get_template_part("sub", "title"); ?>
			<section id="main">
