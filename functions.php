<?php
// css classes
require_once(get_template_directory()."/admin/css-color-classes.php");

//Google Fonts
require_once(get_template_directory()."/admin/themeoptions/functions/googlefonts.php");

// MCE Buttons
require_once(get_template_directory()."/admin/shortcodes/tinymce.button.php");

// Meta boxes
require_once(get_template_directory()."/admin/metaboxes/meta_box.php");

// Theme Option Settings
require_once(get_template_directory()."/admin/themeoptions/index.php");

// Shortcodes
require_once(get_template_directory()."/lib/shortcodes.php");

//Theme Functions
require_once(get_template_directory()."/lib/theme-functions.php");

// nav walker
require_once(get_template_directory()."/lib/class-wp-bootstrap-navwalker.php");

// widgets
require_once(get_template_directory()."/lib/widgets.php");
require_once(get_template_directory()."/admin/plugin-setup.php");

define("ZEETEXTDOMAIN", wp_get_theme()->get("TextDomain"));
define("ZEETHEMENAME", wp_get_theme()->get("Name"));
define("CUSTOM_METABOXES_DIR", get_template_directory_uri()."/admin/metaboxes");

/* content_width - width of media editor popup */
if (!isset($content_width)) {
    $content_width = 600;
}

/* register_nav_menus - register the navigation menus */
register_nav_menus(array(
    "primary"	=> __("Primary", ZEETEXTDOMAIN)
    ,"footer"	=> __("Footer", ZEETEXTDOMAIN))
);

/* sidebars */
register_sidebar(array(
	"name"				=> __("Sidebar", ZEETEXTDOMAIN)
	,"id"				=> "sidebar"
	,"description"		=> __("Widgets in this area will be shown on right side.", ZEETEXTDOMAIN)
	,"before_title"		=> "<h3>"
	,"after_title"		=> "</h3>"
	,"before_widget"	=> "<div>"
	,"after_widget"		=> "</div>")
);
register_sidebar(array(
	"name"				=> __("Bottom", ZEETEXTDOMAIN)
	,"id"				=> "bottom"
	,"description"		=> __("Widgets in this area will be shown before Footer.", ZEETEXTDOMAIN)
	,"before_title"		=> "<h3>"
	,"after_title"		=> "</h3>"
	,"before_widget"	=> "<div class='col-sm-3 col-xs-6'>"
	,"after_widget"		=> "</div>")
);

// Injections

/* theme_enqueue_styles - enqueue global styles */
if (!function_exists("theme_enqueue_styles")) {
	add_action("wp_enqueue_scripts", "theme_enqueue_styles");
	
	function theme_enqueue_styles()
	{
		wp_enqueue_style("theme-bootstrap",	get_template_directory_uri()."/assets/css/bootstrap.min.css");
        wp_enqueue_style("theme-oi", get_template_directory_uri()."/assets/css/open-iconic.min.css");
        wp_enqueue_style("theme-bootstrap-oi", get_template_directory_uri()."/assets/css/open-iconic-bootstrap.min.css", array("theme-bootstrap", "theme-oi"));
        
		wp_enqueue_style("theme-animate", get_template_directory_uri()."/assets/css/animate.min.css");

		wp_enqueue_style("theme-roboto-latin", get_template_directory_uri()."/assets/css/latin.css");
		wp_enqueue_style("theme-roboto-latin-ext", get_template_directory_uri()."/assets/css/latin-ext.css");

        wp_enqueue_style("theme-base", get_template_directory_uri()."/style.css");
	}
}

/* theme_enqueue_scripts - enqueue global scripts */
if (!function_exists("theme_enqueue_scripts")) {
    add_action("wp_enqueue_scripts", "theme_enqueue_scripts");

    function theme_enqueue_scripts()
    {
		wp_enqueue_script("theme-popper", get_template_directory_uri()."/assets/js/popper.min.js");
        wp_enqueue_script("theme-bootstrap", get_template_directory_uri()."/assets/js/bootstrap.min.js", array("jquery", "theme-popper"));
        
        wp_enqueue_script("theme-base", get_template_directory_uri()."/assets/js/main.min.js", array("jquery"));
    }
}

/* theme_login_styles - adds custom stylesheets for the login screen */
if (!function_exists("theme_login_styles")) {
	add_action("login_enqueue_scripts", "theme_login_styles");
	
	function theme_login_styles()
	{
		wp_enqueue_style("theme-login", get_stylesheet_directory_uri()."/style-login.css");
	}
}

/* theme_admin_scripts - adds custom scripts for the admin screen */
if (!function_exists("theme_admin_scripts")) {
	add_action("admin_enqueue_scripts", "theme_admin_scripts");
	
	function theme_admin_scripts()
	{
		wp_enqueue_script("theme-admin", get_template_directory_uri()."/admin/js/admin.js");
	}
}

/* theme_after_setup - post-setup tasks for theme */
if (!function_exists("theme_after_setup")) {
	add_action("after_setup_theme", "theme_after_setup");
	
	function theme_after_setup()
	{
		load_theme_textdomain(ZEETEXTDOMAIN, get_template_directory()."/languages");
		
		add_theme_support("post-formats", array(
			"audio"
			,"gallery"
			,"image"
			,"video"));

		add_theme_support("post-thumbnails");
		add_theme_support("automatic-feed-links");
	}
}

/* theme_attachment_link - add prettyPhoto to attachments? */
if (!function_exists("theme_attachment_link")) {
	add_filter("wp_get_attachment_link", "theme_attachment_link", 10, 5);
	
	function theme_attachment_link($content, $id, $size, $permalink)
	{
		if (!$permalink) {
			$content = preg_replace("/<a/", "<a rel=\"prettyPhoto[gallery]\"", $content, 1);
			return $content;
		}
	}
}

/* theme_title - create the title string */
if (!function_exists("theme_title")) {
	add_filter("wp_title", "theme_title", 10, 2);
	
	function theme_title($title, $sep)
	{
		global $paged, $page;

		if (is_feed())
			return $title;

		$title .= get_bloginfo("name");
		$site_description = get_bloginfo("description", "display");

		if ($site_description && (is_home() || is_front_page()))
			$title = "$title $sep $site_description";

		if ($paged >= 2 || $page >= 2)
			$title = "$title $sep ".sprintf(__("Page %s", ZEETEXTDOMAIN), max($paged, $page));

		$title = str_replace("Genres | ", "", $title);
		return $title;
	}
}

/* theme_password_form - render password form */
if (!function_exists("theme_password_form")) {
	add_filter("the_password_form", "theme_password_form");
	
	function theme_password_form()
	{
		global $post;
		$label = "pwbox-".(empty($post->ID) ? rand() : $post->ID);

		$form = "
			<div class='row'>
				<form action='".esc_url(site_url("wp-login.php?action=postpass", "login_post"))."' method='post'>
					<div class='col-lg-6'>"
						.__("To view this protected post, enter the password below:", ZEETEXTDOMAIN)
						."<div class='input-group'>
							<input class='form-control' name='post_password' placeholder='".__("Password:", ZEETEXTDOMAIN)."' id='".$label."' type='password' />
							<span class='input-group-btn'>
								<button class='btn btn-info' type='submit' name='Submit'>".esc_attr__("Submit", ZEETEXTDOMAIN)."</button>
							</span>
						</div>
					</div>
				</form>
			</div>";
		return $form;
	}
}

/* theme_mce_buttons - create buttons for editor */
if (!function_exists("theme_mce_buttons")) {
	add_filter("mce_buttons", "theme_mce_buttons");
	
	function theme_mce_buttons($mce_buttons)
	{
		$pos = array_search("wp_more", $mce_buttons, true);

		if ($pos !== false) {
			$buttons = array_slice($mce_buttons, 0, $pos + 1);
			$buttons[] = "wp_page";
			$mce_buttons = array_merge($buttons, array_slice($mce_buttons, $pos + 1));
		}
		
		return $mce_buttons;
	}
}

/* theme_init_sliders - create slider post type */
if (!function_exists("theme_init_sliders")) {
	add_action("init", "theme_init_sliders");
	
	function theme_init_sliders()
	{
		$labels = array(
			"name"					=> __("Slider", ZEETEXTDOMAIN)
			,"singular_name"		=> __("Slider", ZEETEXTDOMAIN)
			,"menu_name"			=> __("Sliders", ZEETEXTDOMAIN)
			,"all_items"			=> __("All Sliders", ZEETEXTDOMAIN)
			,"add_new"				=> __("Add New", ZEETEXTDOMAIN)
			,"add_new_item"			=> __("Add New Slider", ZEETEXTDOMAIN)
			,"edit_item"			=> __("Edit Slider", ZEETEXTDOMAIN)
			,"new_item"				=> __("New Slider", ZEETEXTDOMAIN)
			,"view_item"			=> __("View Slider", ZEETEXTDOMAIN)
			,"search_items"			=> __("Search Portfolios", ZEETEXTDOMAIN)
			,"not_found"			=> __("No item found", ZEETEXTDOMAIN)
			,"not_found_in_trash"	=> __("No item found in Trash", ZEETEXTDOMAIN));

		$args = array(
			"labels"				=> $labels
			,"public"				=> true
			,"has_archive"			=> false
			,"exclude_from_search"	=> true
			,"menu_icon"			=> get_template_directory_uri()."/admin/images/icon-slider.png"
			,"rewrite"				=> true
			,"capability_type"		=> "post"
			,"supports"				=> array("title", "page-attributes", "editor", "thumbnail"));
			
		register_post_type("zee_slider", $args);
		flush_rewrite_rules();
	}
}

/* theme_headertext - substitute in login-page header text */
if (!function_exists("theme_headertext")) {
	add_filter("login_headertext", "theme_headertext");

	function theme_headertext($headertext)
	{
		$headertext = esc_html__(get_bloginfo("name"), ZEETEXTDOMAIN);
		return $headertext;
	}
}

/* deactivate default gallery css */
add_filter("use_default_gallery_style", "__return_false");

// Injections End

// Functions

/* zee_option - retrieve an option value */
if (!function_exists("zee_option")) {
    function zee_option($index = false, $index2 = false)
    {
        global $data;

        if ($index2)
            return isset($data[$index]) && isset($data[$index][$index2]) ? $data[$index][$index2] : "";
        else
            return isset($data[$index]) ? $data[$index] : "";
    }
}

/* zee_pagination - display paginated links */
if (!function_exists("zee_pagination")) {
	function zee_pagination()
	{
		global $wp_query;
		
		if ($wp_query->max_num_pages > 1) {
            $big = 999999999; // need an unlikely integer
            $items =  paginate_links(array(
				"base"			=> str_replace($big, "%#%", esc_url(get_pagenum_link($big)))
				,"format"		=> "?paged=%#%"
				,"prev_next"	=> true
				,"current"		=> max(1, get_query_var("paged"))
				,"total"		=> $wp_query->max_num_pages
				,"type"			=> "array"));

            $pagination = "<ul class='pagination'>\n\t<li>";
            $pagination .= join("</li>\n\t<li>", $items);
            $pagination .= "</li>\n</ul>\n";

            return $pagination;
        }
        
        return;
    }
}

/* zee_post_nav - display post navigation */
if (!function_exists("zee_post_nav")) {
	function zee_post_nav()
	{
		global $post;

		$previous = is_attachment() ? get_post($post->post_parent) : get_adjacent_post(false, "", true);
		$next = get_adjacent_post(false, "", false);

		if (!$next && !$previous)
			return;

?>
		<nav class="navigation post-navigation" role="navigation">
			<div class="pager">
<?php
				if ($previous) {
?>
					<li class="previous">
						<?php previous_post_link("%link", _x("<i class='oi oi-arrow-left'></i> %title", "Previous post link", ZEETEXTDOMAIN)); ?>
					</li>
<?php
				}

				if ($next) {
?>
					<li class="next">
						<?php next_post_link("%link", _x("%title <i class='oi oi-arrow-right'></i>", "Next post link", ZEETEXTDOMAIN)); ?>
					</li>
<?php
				}
?>
			</div>
		</nav>
<?php
	}
}

/* zee_link_pages - create before and after page links */
if (!function_exists("zee_link_pages")) {
    function zee_link_pages($args = '')
    {
        $defaults = array(
			"before"			=> ""
			,"after"			=> ""
			,"link_before"		=> ""
			,"link_after"		=> ""
			,"next_or_number"	=> "number"
			,"nextpagelink"		=> __("Next page", ZEETEXTDOMAIN)
			,"previouspagelink" => __("Previous page", ZEETEXTDOMAIN)
			,"pagelink"			=> "%"
			,"echo"				=> 1);

        $r = wp_parse_args($args, $defaults);
        $r = apply_filters("wp_link_pages_args", $r);
        extract($r, EXTR_SKIP);
        
		global $page, $numpages, $multipage, $more, $pagenow;

        $output = "";
        if ($multipage) {
            if ("number" == $next_or_number) {
				$output .= $before."<ul class='pagination'>";
                $laquo = $page == 1 ? "class='disabled'" : "";
                $output .= "<li ".$laquo.">"._wp_link_page($page - 1)."&laquo;</li>";
                for ($i = 1; $i < ($numpages + 1); $i++) {
                    $j = str_replace("%", $i, $pagelink);

                    if ($i != $page || (!$more && $page == 1)) {
                        $output .= "<li>";
                        $output .= _wp_link_page($i);
                    } else {
                        $output .= "<li class='active'>";
                        $output .= _wp_link_page($i);
                    }
                    
					$output .= $link_before.$j.$link_after;
                    $output .= "</li>";
                }
                $raquo = $page == $numpages ? "class='disabled'" : "";
                $output .= "<li ".$raquo.">"._wp_link_page($page + 1)."&raquo;</li>";
                $output .= "</ul>".$after;
            } else {
                if ($more) {
                    $output .= $before."<ul class='pager'>";
                    $i = $page - 1;
                    if ($i && $more) {
                        $output .= "<li class='previous'>"._wp_link_page($i);
                        $output .= $link_before.$previouspagelink.$link_after."</li>";
                    }
                    $i = $page + 1;
                    if ($i <= $numpages && $more) {
                        $output .= "<li class='next'>"._wp_link_page($i);
                        $output .= $link_before.$nextpagelink.$link_after."</li>";
                    }
                    $output .= "</ul>".$after;
                }
            }
        }

        if ($echo)
            echo $output;
        else
            return $output;
    }
}

/* zee_get_thumb_url - get post thumbnail */
if (!function_exists("zee_get_thumb_url")) {
	function zee_get_thumb_url($post_ID)
	{
		return wp_get_attachment_url(get_post_thumbnail_id($post_ID));
	}
}

/* zee_get_avatar_url - get avatar url */
if (!function_exists("zee_get_avatar_url")) {
	function zee_get_avatar_url($get_avatar)
	{
		preg_match("/src='(.*?)'/i", $get_avatar, $matches);
		return $matches[1];
	}
}

/* zee_comments_list - render comment list */
if (!function_exists("zee_comments_list")) {
	function zee_comments_list($comment, $args, $depth)
	{
		$GLOBALS["comment"] = $comment;
		switch ($comment->comment_type) {
			case "pingback":
			case "trackback":
?>
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<p>
<?php
						_e("Pingback:", ZEETEXTDOMAIN);
						comment_author_link();
						edit_comment_link(__("(Edit)", ZEETEXTDOMAIN), "<span class='edit-link'>", "</span>"); 
?>
					</p>
<?php
				break;
			default:
				global $post;
?>
				<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
					<div id="comment-<?php comment_ID(); ?>" class="comment media">
						<div class="float-left comment-author vcard">
<?php
							$get_avatar = get_avatar($comment->comment_author_email, 48);
							$avatar_img = zee_get_avatar_url($get_avatar);
							global $comment_authors;
							if($comment->comment_parent == 0)
								$comment_authors[] = $comment->user_id;
?>
							<img class="avatar img-circle" src="<?php echo $avatar_img; ?>" alt="">
						</div>
						<div class="media-body">
							<div class="well">
								<div class="comment-meta media-heading">
									<span class="author-name">
										<?php _e("By", ZEETEXTDOMAIN); ?> <strong><?php echo get_comment_author(); ?></strong>
									</span>
									-
									<time datetime="<?php echo get_comment_date(); ?>">
										<?php echo get_comment_date(); ?> <?php echo get_comment_time(); ?>
										<?php edit_comment_link(__("Edit", ZEETEXTDOMAIN), "<small class='edit-link'>", "</small>"); ?>
									</time>
									<span class="reply float-right">
<?php
										comment_reply_link(array_merge($args, array(
											"reply_text"	=> sprintf(__("%s Reply", ZEETEXTDOMAIN), "<i class='oi oi-reload'></i>")
											,"depth"		=> $depth
											,"max_depth"	=> $args["max_depth"])));
?>
									</span>
								</div>
<?php
								if ($comment->comment_approved == "0") {
?>
									<div class="alert alert-info"><?php _e("Your comment is awaiting moderation.", ZEETEXTDOMAIN); ?></div>
<?php
								}
?>
								<div class="comment-content comment">
									<?php comment_text(); ?>
								</div>
							</div>
						</div>
					</div>
<?php
				break;
		}
	}
}

/* zee_comment_form - render comment form */
if (!function_exists("zee_comment_form")) {
	function zee_comment_form($args = array(), $post_id = null)
	{
		if ($post_id === null)
			$post_id = get_the_ID();
		else
			$id = $post_id;

		$commenter = wp_get_current_commenter();
		$user = wp_get_current_user();
		$user_identity = $user->exists() ? $user->display_name : "";

		if (!isset($args["format"]))
			$args["format"] = current_theme_supports("html5", "comment-form") ? "html5" : "xhtml";
		
		$req = get_option("require_name_email");
		$aria_req = ($req ? " aria-required='true'" : "");
		$html5 = "html5" === $args["format"];

		$fields   =  array(
			"author"	=> "
				<div class='form-group'>
					<div class='col-sm-6 comment-form-author'>
						<input id='author' class='form-control' type='text' name='author' placeholder='".__("Name", ZEETEXTDOMAIN)."' value='".esc_attr($commenter["comment_author"])."' ".$aria_req." />
					</div>"
			,"email"	=> "
					<div class='col-sm-6 comment-form-email'>
						<input id='email' class='form-control' ".($html5 ? "type='email'" : "type='text'")." name='email' placeholder='".__("Email", ZEETEXTDOMAIN)."' value='".esc_attr($commenter["comment_author_email"])."' ".$aria_req." />
					</div>
				</div>"
			,"url"		=> "
				<div class='form-group'>
					<div class='col-sm-12 comment-form-url'>
						<input id='url' class='form-control' ".($html5 ? "type='url'" : "type='text'")." name='url' placeholder='".__("Website", ZEETEXTDOMAIN)."' value='".esc_attr($commenter["comment_author_url"])."' />
					</div>
				</div>");

		$required_text = sprintf(" ".__("Required fields are marked %s", ZEETEXTDOMAIN), "<span class='required'>*</span>");

		$defaults = array(
			"fields"			=> apply_filters("comment_form_default_fields", $fields)
			,"comment_field"	=> "
				<div class='form-group comment-form-comment'>
					<div class='col-sm-12'>
						<textarea class='form-control' id='comment' name='comment' placeholder='"._x("Comment", "noun", ZEETEXTDOMAIN)."' rows='8' aria-required='true'></textarea>
					</div>
				</div>"
			,"must_log_in"		=> "
				<div class='alert alert-danger must-log-in'>"
					.sprintf(__("You must be <a href='%s'>logged in</a> to post a comment."), wp_login_url(apply_filters("the_permalink", get_permalink($post_id))))
				."</div>"
			,"logged_in_as"		=> "
				<div class='alert alert-info logged-in-as'>"
					.sprintf(__("Logged in as <a href='%s'>%s</a>. <a href='%s' title='Log out of this account'>Log out?</a>", ZEETEXTDOMAIN)
						,get_edit_user_link()
						,$user_identity
						,wp_logout_url(apply_filters("the_permalink", get_permalink($post_id))))
					."</div>"
			,"comment_notes_before"	=> "
				<div class='alert alert-info comment-notes'>"
					.__("Your email address will not be published.", ZEETEXTDOMAIN).($req ? $required_text : "")
				."</div>"
			,"comment_notes_after"	=> "
				<div class='form-allowed-tags'>"
					.sprintf(__("You may use these <abbr title='HyperText Markup Language'>HTML</abbr> tags and attributes: %s", ZEETEXTDOMAIN)
					,"<code>"
						.allowed_tags()
					."</code>")
				."</div>"
			,"id_form"				=> "commentform"
			,"id_submit"			=> "submit"
			,"title_reply"			=> __("Leave a Reply", ZEETEXTDOMAIN)
			,"title_reply_to"		=> __("Leave a Reply to %s", ZEETEXTDOMAIN)
			,"cancel_reply_link"	=> __("Cancel reply", ZEETEXTDOMAIN)
			,"label_submit"			=> __("Post Comment", ZEETEXTDOMAIN)
			,"format"				=> "xhtml");

		$args = wp_parse_args($args, apply_filters("comment_form_defaults", $defaults));

		if (comments_open($post_id)) {
			do_action("comment_form_before");
?>
			<div id="respond" class="comment-respond pb-2">
				<h3 id="reply-title" class="comment-reply-title">
					<?php comment_form_title($args['title_reply'], $args['title_reply_to'] );?>
					<small><?php cancel_comment_reply_link( $args['cancel_reply_link']); ?></small>
				</h3>
<?php
				if (get_option("comment_registration") && !is_user_logged_in()) {
					echo $args["must_log_in"];
					do_action("comment_form_must_log_in_after");
				} else {
?>
					<form action="<?php echo site_url("/wp-comments-post.php"); ?>" method="post" id="<?php echo esc_attr($args["id_form"]); ?>" class="form-horizontal comment-form"<?php echo $html5 ? " novalidate" : ""; ?> role="form">
<?php
						do_action("comment_form_top");
						if (is_user_logged_in()) {
							echo apply_filters("comment_form_logged_in", $args["logged_in_as"], $commenter, $user_identity);
							do_action("comment_form_logged_in_after", $commenter, $user_identity);
						} else {
							echo $args["comment_notes_before"];
							do_action("comment_form_before_fields");
							
							foreach ((array) $args["fields"] as $name => $field) {
								echo apply_filters("comment_form_field_{$name}", $field)."\n";
							}
							do_action("comment_form_after_fields");
						}
						echo apply_filters("comment_form_field_comment", $args["comment_field"]);
						echo $args["comment_notes_after"];
?>
						<div class="form-submit">
							<input class="btn btn-danger btn-lg" name="submit" type="submit" id="<?php echo esc_attr($args["id_submit"] ); ?>" value="<?php echo esc_attr($args["label_submit"]); ?>" />
							<?php comment_id_fields($post_id); ?>
						</div>
						<?php do_action("comment_form", $post_id); ?>
					</form>
<?php						
				}
?>
			</div>
<?php
			do_action("comment_form_after");
		} else {
			do_action("comment_form_comments_closed");
		}
	}
}

/* zee_the_attached_image - display an image */
if (!function_exists("zee_the_attached_image")) {
	function zee_the_attached_image()
	{
		$post = get_post();
		$attachment_size = array(724, 724);
		$next_attachment_url = wp_get_attachment_url();

		/**
		* Grab the IDs of all the image attachments in a gallery so we can get the URL
		* of the next adjacent image in a gallery, or the first image (if we're
		* looking at the last image in a gallery), or, in a gallery of one, just the
		* link to that image file.
		*/
		$attachment_ids = get_posts(array(
			"post_parent"		=> $post->post_parent
			,"fields"			=> "ids"
			,"numberposts"		=> -1
			,"post_status"		=> "inherit"
			,"post_type"		=> "attachment"
			,"post_mime_type"	=> "image"
			,"order"			=> "ASC"
			,"orderby"			=> "menu_order ID"));

		// If there is more than 1 attachment in a gallery...
		if (count($attachment_ids) > 1) {
			foreach ($attachment_ids as $attachment_id) {
				if ($attachment_id == $post->ID) {
					$next_id = current($attachment_ids);
					break;
				}
			}

			if ($next_id)
				$next_attachment_url = get_attachment_link($next_id);
			else
				$next_attachment_url = get_attachment_link(array_shift($attachment_ids));
		}

		printf("<a href='%1$s' title='%2$s' rel='attachment'>%3$s</a>"
			,esc_url($next_attachment_url)
			,the_title_attribute(array("echo" => false))
			,wp_get_attachment_image($post->ID, $attachment_size));
	}
}

// Functions End

/* slider fields */
$prefix = "slider_";
$fields = array(array(
		"label"	=> __("Background Image", ZEETEXTDOMAIN)
		,"desc"	=> __("Show background image in slider", ZEETEXTDOMAIN)
		,"id"	=> $prefix."background_image"
		,"type"	=> "image")
	,array(
		"label"	=> __("Button Text", ZEETEXTDOMAIN)
		,"desc"	=> __("Show Slider Button and Button Text", ZEETEXTDOMAIN)
		,"id"	=> $prefix."button_text"
		,"type"	=> "text")
	,array(
		"label"	=> __("Button URL", ZEETEXTDOMAIN)
		,"desc"	=> __("Slider URL link.", ZEETEXTDOMAIN)
		,"id"	=> $prefix."button_url"
		,"type"	=> "text")
	,array(
		"label"	=> __("Boxed Style", ZEETEXTDOMAIN)
		,"desc"	=> __("Show boxed Style.", ZEETEXTDOMAIN)
		,"id"	=> $prefix."boxed"
		,"type"	=> "select"
		,"options"	=> array(array(
				"value"		=> "no"
				,"label"	=> __("No", ZEETEXTDOMAIN))
			,array(
				"value"		=> "yes"
				,"label"	=> __("Yes", ZEETEXTDOMAIN))))
	,array(
		"label"	=> __("Position", ZEETEXTDOMAIN)
		,"desc"	=> __("Show slider Position.", ZEETEXTDOMAIN)
		,"id"	=> $prefix."position"
		,"type"	=> "select"
		,"options"	=> array(array(
				"value"		=> "left"
				,"label"	=> __("Left", ZEETEXTDOMAIN))
			,array(
				"value"		=> "center"
				,"label"	=> __("Center", ZEETEXTDOMAIN))
			,array(
				"value"		=> "right"
				,"label"	=> __("Right", ZEETEXTDOMAIN)))));

$fields_video = array(array(
		"label"	=> __("Video Type", ZEETEXTDOMAIN)
		,"desc"	=> __("Select video type.", ZEETEXTDOMAIN)
		,"id"	=> $prefix."video_type"
		,"type"	=> "radio"
		,"options"	=> array(array(
				"value"		=> ""
				,"label"	=> __("None", ZEETEXTDOMAIN))
			,array(
				"value"		=> "youtube"
				,"label"	=> __("Youtube", ZEETEXTDOMAIN))
			,array(
				"value"		=> "vimeo"
				,"label"	=> __("Vimeo", ZEETEXTDOMAIN))))
	,array(
		"label"	=> __("Video Link", ZEETEXTDOMAIN)
		,"desc" => __("Video link", ZEETEXTDOMAIN)
		,"id"	=> $prefix."video_link"
		,"type"	=> "text"));
new Custom_Add_Meta_Box("zee_slider_box", __("Slider Settings", ZEETEXTDOMAIN), $fields, "zee_slider", true);
new Custom_Add_Meta_Box("zee_slider_box_video", __("Video Settings", ZEETEXTDOMAIN), $fields_video, "zee_slider", true);

/* subtitle fields */
$prefix = 'page_';
$fields = array(array(
		"label"	=> __("Subtitle", ZEETEXTDOMAIN)
		,"id"	=> $prefix."subtitle"
		,"type"	=> "text"));
new Custom_Add_Meta_Box("zee_page_box", __("Subtitle Options", ZEETEXTDOMAIN), $fields, "page", true);
